Here are simple notebooks to introduce neural nets and CNN on a classical digit recognition task (in Keras) 

See also the following slides + notebooks on advanded deep learning methods 
- https://github.com/nkeriven/ensta-mt12/tree/main/slides
- https://github.com/nkeriven/ensta-mt12/tree/main/notebooks
