# Lab 4 statement

The objective of this lab is to illustrate Lasso-type (L1) regularization and variable selection
procedures for (generalized) linear models.

_Note: For each notebook, read the cells and run the code, then follow the instructions/questions in the `Questions` or `Exercise` cells._

See the notebooks in the [5bis_linear_models_lasso_logistic](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/5bis_linear_models_lasso_logistic/) folder:


1. Experiment the effect of lasso (L1) regularization [`N1_L1_regularization.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/5bis_linear_models_lasso_logistic/N1_L1_regularization.ipynb)

2. Perform Lasso penalized Logistic Regression, and greedy variable selection procedures, to model the risk of coronary heart disease based on clinical data [`N3_LR_heart_diseases_SA.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/5bis_linear_models_lasso_logistic/N3_LR_heart_diseases_SA.ipynb)

3. Perform Lasso regression for a high-dimensional sparse model based on the Advertising data set
 [`N2_lasso_curse_dimensionality.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/5bis_linear_models_lasso_logistic/N2_lasso_curse_dimensionality.ipynb)

*Note: The first notebook is an interactive demonstration on the tensorflow playground
and can be skipped for the class session*
